<?php

/*

---------------------------------------
License Setup
---------------------------------------

Please add your license key, which you've received
via email after purchasing Kirby on http://getkirby.com/buy

It is not permitted to run a public website without a
valid license key. Please read the End User License Agreement
for more information: http://getkirby.com/license

*/

c::set('license', 'put your license key here');

/*

---------------------------------------
Kirby Configuration
---------------------------------------

By default you don't have to configure anything to
make Kirby work. For more fine-grained configuration
of the system, please check out http://getkirby.com/docs/advanced/options

*/

c::set('debug', true);
c::set('panel.install', true);

c::set('languages', array(
	array(
		'code'		=> 'fr',
		'name'		=> 'FR',
		'default' => true,
		'locale'	=> 'fr_FR',
		'url'		 => '/',
	),
	array(
		'code'		=> 'en',
		'name'		=> 'EN',
		'locale'	=> 'en_US',
		'url'		 => '/en',
	),
	array(
		'code'		=> 'es',
		'name'		=> 'ES',
		'locale'	=> 'es_Es',
		'url'		 => '/es',
	),
));

c::set('language.detect', true);

c::set("kirbyGalleryGrid", "l");

c::set('routes', array(
	array(
		'pattern' => 'p',
		'action'  => function() {
			go('/');
		}
	),
));


c::set('home', 'projects');

c::set('panel.widgets', array(
	'pages'    => true,
	'site'     => false,
	'account'  => false,
	'history'  => true
));


// Copy this code into your config.php file
// Replace the default $maxDimension to meet your needs
// Read more about Kirby's Panel hooks at https://getkirby.com/docs/panel/developers/hooks
// Shrink large images on upload

// https://gist.github.com/samnabi/f4ecb351f2ec2bd596e5
kirby()->hook('panel.file.upload', 'shrinkImage');
kirby()->hook('panel.file.replace', 'shrinkImage');
function shrinkImage($file, $maxDimension = 1800) {

	$limitedPagesTypes = array('project');

	try {
		$pageTemplate = $file->page()->intendedTemplate();
		if (in_array($pageTemplate, $limitedPagesTypes) ) {
			if ($file->type() == 'image' and ($file->width() > $maxDimension or $file->height() > $maxDimension)) {

				// Get original file path
				$originalPath = $file->dir().'/'.$file->filename();
				// Create a thumb and get its path
				$resized = $file->resize($maxDimension,$maxDimension);
				$resizedPath = $resized->dir().'/'.$resized->filename();
				// Replace the original file with the resized one
				copy($resizedPath, $originalPath);
				unlink($resizedPath);
			}
		}

	} catch(Exception $e) {
		return response::error($e->getMessage());
	}
}


