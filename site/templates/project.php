<?php $ajax = ( kirby()->request()->ajax() ? true : false ); ?>
<?php if($ajax == false): ?>

	<?php snippet('header', array('bodyclass'=>'modal-open')) ?>
	<main class="main" role="main">
		<?php snippet('projects', array( 'page' => page('projects') ) ); ?>
	</main>

	<?php snippet('modals', array('content' => snippet('project/overview', true, true))) ?>

	<?php snippet('footer') ?>

<?php else: ?>
	<?php snippet('project/overview') ?>
<?php endif; ?>
