<?php snippet('header') ?>

	
	<?php 
		$videos = array('10h', '16h', '20h', '23h');
		$video = $videos[rand (0, 3)];

	?>

	<video id="bg-video" class="video-js" preload="auto" width="100%" height="100%" data-setup="{}" autoplay loop>
		<source src="assets/videos/<?=$video?>.mp4" type='video/mp4'>
		<source src="assets/videos/<?=$video?>.webm" type='video/webm'>
		<p class="vjs-no-js">
			To view this video please enable JavaScript, and consider upgrading to a web browser that
			<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
		</p>
	</video>


	<main class="main" role="main">
		<?php snippet('projects', array( 'page' => $page ) ); ?>
	</main>

	<?php snippet('modals') ?>


<?php snippet('footer') ?>

