
<?php if(!isset($content)) { $content = null; } ?>

<div id="overview" class="modal <?php e(!$content, 'hidden') ?>">
	<div class="modal-wrapper">
		<?php echo $content ?>
	</div>
	<a href="<?=$site->url()?>" class="big cross close overview" data-title="<?=$site->title()?>"></a>
</div>