<section class="projects">
	<?php foreach ($page->children()->visible()->shuffle() as $project) : ?>
		<article class="project">
			<figure class="fadable faded">
				<?php if ($project->cover()->isNotEmpty() && $project->cover()->toFile()) {
					$cover =  $project->cover()->toFile();
				} else {
					$cover = $project->images()->sortBy('sort', 'asc')->first();
				} ?>
				<?php echo thumb($cover, array(
					'width' => 600,
					'height' => 400
				)); ?>
			</figure>
			<a class="link" href="<?= $project->url() ?>" data-title="<?= $project->title() ?>">
				<span class="title"><?= $project->title() ?></span>			
			</a>
		</article>

	<?php endforeach ?>
</section>
