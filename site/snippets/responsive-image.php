
<?php 
	if (isset($crop) == false){ $crop = false ; }
	$sources = array();

	foreach ($sizes as $size){
		$width = $size[0];
		$height = $size[1];
		$thumb = thumb($image, array('width' => $width, 'height' => $height, 'crop' =>$crop ))->url();
		$source = array('width' => $width, 'height' => $height, 'thumb' => $thumb );
		array_push($sources, $source);
	}

?>

<picture>
	<?php foreach ($sources as $source): ?>
		<source srcset="<?= $source['thumb'] ?>"  media="(min-width: <?= $source['width'] ?>px)">	
	<?php endforeach ?>

	<img 	srcset="<?= $sources[0]['thumb'] ?>  <?= $sources[0]['width'].'w'?>"
			alt="<?php e($image->legende()->isNotEmpty(), $image->legende(), $image->title()) ?>"
  		  >
</picture>

