<header id="header" role="banner" class="fadable faded" data-timing="100">

	<nav id="menu" role="navigation">
		<ul class="menu-items">
			<?php foreach($pages->visible() as $item): ?>
			<li class="menu-item <?php e($item->isOpen(), 'active') ?>">
				<a href="<?= $item->url() ?>">
					<span><?= $item->title()->html() ?></span>
				</a>
			</li>
			<?php endforeach ?>
		</ul>
	</nav>

	<div id="logo">
		<a href="<?= url() ?>" rel="home"><?= $site->title()->html() ?></a>
	</div>

	<nav id="languages" role="navigation">
		<ul>
			<?php foreach($site->languages() as $language): ?>
			<li<?php e($site->language() == $language, ' class="active"') ?>>
				<a href="<?php echo $page->url($language->code()) ?>">
					<span><?php echo html($language->name()) ?></span>
				</a>
			</li>
			<?php endforeach ?>
		</ul>
	</nav>

</header>