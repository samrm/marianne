<!doctype html>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<title><?= $site->title()->html() ?> | <?= $page->title()->html() ?></title>
	<meta name="description" content="<?= $site->description()->html() ?>">

	<?= css(array(
		'assets/libs/swiper/css/swiper.min.css',
		'assets/css/public.css',
	)) ?>

</head>
<?php if (!isset($bodyclass)) { $bodyclass = false; } ?>
<body class="blurable <?php e($bodyclass, $bodyclass) ?>" data-path="<?=$site->url()?>">
	

<?php snippet('navigation') ?>
