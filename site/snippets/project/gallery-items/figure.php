<figure class="swiper-slide" data-index="<?=$index?>" data-hash="<?=$index?>">
	<?php snippet('responsive-image', 
		array('image'=>$item, 'sizes'=>[[1280,960], [640,960], [320,640]] )
	); ?>
	<?php if ($item->caption()->isNotEmpty()): ?>
		<figcaption class="caption hidden"><?php echo $item->caption()->kirbytext() ?></figcaption>
	<?php endif ?>
</figure>

