<div id="gallery" class="modal hidden">
	<div class="modal-wrapper">
		<div class="modal-content">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<?php $index = 1; ?>
					<?php foreach ($page->gallery()->yaml() as $item){
						if($page->image($item) && $item = $page->image($item)){
							snippet('project/gallery-items/figure', array('item' => $item, 'index' => $index));
						}
						elseif ($page->find($item) && $item = $page->find($item)) {
							snippet('project/gallery-items/article', array('item' => $item, 'index' => $index));
						} 
						$index += 1;
					}?>
				</div>
			</div>
			<div class="pagination"></div>
		</div>
	</div>
	<button class="big cross close gallery"></button>
</div>