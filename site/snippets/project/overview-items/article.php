
<article class="thumb fadable faded" data-index="<?=$index?>">
	<div class="article-wrapper">
		<div class="article-content text"><?= str::excerpt($item->text(), 280, true, '[…]'); ?></div>
	</div>
</article>