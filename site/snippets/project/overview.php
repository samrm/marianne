<div class="modal-content blurable overview">
	<h1><?= $page->title() ?></h1>
	<?php $index = 1; ?>
	<?php foreach ($page->gallery()->yaml() as $item){ 
		if($page->image($item) && $item = $page->image($item)){
			snippet('project/overview-items/figure', array('item' => $item, 'index' => $index));
		}
		elseif ($page->find($item) && $item = $page->find($item)) {
			snippet('project/overview-items/article', array('item' => $item, 'index' => $index));
		} 
		$index += 1;
	} ?>
</div>

<?php snippet('project/gallery'); ?>
