Description: 

----

Sort: 3

----

Caption: 

Ce travail est né à Buenos Aires, lors d’un voyage où je ne cherchais rien et j’ai retrouvé la mémoire du pays perdu.
Tous ceux qui vivent là-bas ont en eux le déplacement, la terre lointaine et rêvée, des racines qui ne s’ancrent pas dans ce sol, là, celui où ils sont. 

Les fantômes que je n’avais pas encore rencontrés ont pris corps.

J’étais chez moi.