
var M;
$(document).ready(function(){
	M = new Website();
});


function Website(){
	this.init();
};
Website.prototype = {
	init: function(){ var self = this;
		self.dom = {
			projects: {
				wrapper: $('section.projects'),
			},
		};
		self.basepath = $('body').data('path');
		self.enableRouter();
		self.enableRandomPlacement();
		self.enableOverview();
		self.enableGallery();
		self.enableControls();
		self.enableVideo();


		var initialize = function(){
			if (self.dom.overview.hasClass('hidden') == false) {
				self.view = window.location.hash ? self.gallery : self.overview;
				self.setState(self.dom.overview.find('.modal-wrapper').first().html(), document.title, document.location.href, self.view.title, true);
			}
			if (self.dom.overview.find('.swiper-container').length) {
				// if gallery open gallery
				var index = window.location.hash ? parseInt(window.location.hash.substr(1)) : 0 ;
				self.gallery.init(index);
				if (window.location.hash) {self.gallery.open(index, false)}
			}

		}
		self.reveal(initialize());
	},
	reveal: function(callback){ var self = this;
		$('.fadable.faded').each(function(){
			var $elem = $(this), $img = $elem.find('img');
			if ($img.length) {
				$img.on('load', function(){
					$elem.removeClass('faded');
				});
				setTimeout(function() { $elem.removeClass('faded');}, 1000);
			} else {
				setTimeout(function() { $elem.removeClass('faded');}, 100);
			}
		}).promise().done( function(){
			if (typeof callback !== 'undefined') callback();
		});
	},
	enableRouter: function(){ var self = this;
		self.setState = function(html, title, url, view, replace = false){
			document.title = title;
			if (replace) {
				window.history.replaceState({"html":html, "pageTitle":title, 'url': url, 'view': view},"", url);
			} else {
				window.history.pushState({"html":html, "pageTitle":title, 'url': url, 'view': view},"", url);
			}
		}
		
		window.onpopstate = function(e){
			if (e.state) {
				switch(e.state.view){
					case 'projects':
						self.overview.close(false);
						break;
					case 'overview':
						if(e.state.html) {
							self.dom.overview.find('.modal-wrapper').first().html(e.state.html);
						}
						self.overview.open(false);
					case 'gallery':
						if(!window.location.hash){ 
							self.gallery.close(false);
						} else {
							if (self.gallery.opened == false){
								var index = window.location.hash ? parseInt(window.location.hash.substr(1)) : 0 ;
								self.gallery.swiper.slideTo(index, 0);
								self.gallery.open(index, false);
							}
						}
						break;
				}
			}
		};
	},
	enableControls: function(){ var self = this;
		self.enableKeys();
		self.dom.projects.wrapper.off('click').on('click', '.project a.link', function(e){
			e.preventDefault();
			self.overview.load($(this));
		});
	},
	enableOverview: function(){ var self = this;
		self.overview = {title:'overview'};
		self.dom.overview =  $('#overview');
		self.dom.overview.on('click', '.thumb', function(e){
			var index = $(this).data('index');
			self.gallery.open(index);
		});
		self.dom.overview.on('click', '.close.overview', function(e){
			e.preventDefault();
			self.overview.close();
		});

		self.overview.open = function(history = true){
			$('body').addClass('modal-open');
			self.dom.overview.removeClass('hidden');
			self.reveal(function(){
				self.gallery.init(0);
			});
			self.view = self.overview;
			if (history) self.setState(self.overview.html, self.overview.project, self.overview.url, self.view.title);
		}
		self.overview.close = function(history = true){
			self.dom.overview.addClass('hidden');
			$('body').removeClass('modal-open');
			self.gallery.destroy();
			self.view = null;
			if (history) self.setState('', document.title, self.basepath, 'projects');
		}
		self.overview.load = function($project){
			$('body').addClass('modal-open');
			self.overview.url = $project.attr('href');
			var callback = function(data){
				self.overview.html = data;
				self.overview.project = $project.data('title');
				self.dom.overview.find('.modal-wrapper').first().html(data);
				self.overview.open();
			}
			self.ajaxCall(self.overview.url, callback);
		}

	},
	enableGallery: function(){ var self = this;
		self.gallery = {title:'gallery'};
		self.gallery.init = function(index = 0){
			self.dom.gallery = self.dom.overview.find('#gallery');
			self.dom.gallery.swiper = self.dom.gallery.find('.swiper-container');

			self.gallery.swiper = new Swiper(self.dom.gallery.swiper, {
				speed: 400,
				spaceBetween: 0,
				loop: true,
				pagination: '.pagination',
				paginationType: 'fraction',
				nextButton: '.swiper-slide',
				observer: true,
				initialSlide: index,
				grabCursor: true,
				hashnav: true,
				hashnavWatchState: true,
				onSliderMove: function(swiper){
					swiper.wrapper.find('.caption:not(.hidden)').addClass('hidden');
				},
				onSetTransition: function(swiper){
					swiper.wrapper.find('.swiper-slide:not(.swiper-slide-active) .caption:not(.hidden)').addClass('hidden');
				},
				onTransitionEnd: function(swiper){
					swiper.wrapper.find('.swiper-slide-active .caption.hidden').removeClass('hidden');
				},
			});

			// controls
			self.dom.gallery.on('click', '.close.gallery', function(e){
				e.preventDefault();
				self.gallery.close();
			});
		}
		self.gallery.open = function(index, history = true){
			self.dom.overview.find('.modal-content').first().addClass('modal-open');
			self.dom.gallery.removeClass('hidden');
			self.gallery.swiper.update();
			var url = window.location.pathname+'#'+index;
			self.view = self.gallery;
			if (history) self.setState('', document.title, url, self.view.title );
			self.gallery.opened = true;
			self.gallery.swiper.slideTo(index, 0);
		}
		self.gallery.close = function(history = true ){
			self.dom.gallery.addClass('hidden');
			self.dom.overview.find('.modal-content').first().removeClass('modal-open');
			self.view = self.overview;
			if (history) self.setState(self.overview.html, document.title, window.location.pathname, self.view.title);
			self.gallery.opened = false;
		}
		self.gallery.prev = function(){
			self.gallery.swiper.slidePrev();
		}
		self.gallery.next = function(){
			self.gallery.swiper.slideNext();
		}
		self.gallery.destroy = function(){
			self.gallery.swiper.destroy();
			self.dom.gallery = null;
		}
	},
	ajaxCall: function(url, callback){ var self = this;
		$.ajax({
			type: 'GET',
			url:  url,
			contentType: 'application/html; charset=utf-8',
			dataType: 'html',
			success: function(data){
				if (typeof callback != 'undefined') callback(data);
			},
			error: function(data){
				console.log(data);
			}
		});
	},
	enableKeys: function(){ var self = this;
		$(document).on('keydown', function(e){
			switch (e.which) {
				case 39: // right
					if (self.view && ('next' in self.view)) self.view.next();
					break;
				case 37: // left
					if (self.view && ('prev' in self.view)) self.view.prev();
					break;
				case 27: // esc
					if (self.view && ('close' in self.view)) self.view.close();
					break;
			}
			
		});
	},
	enableRandomPlacement: function(){ var self = this;
		var isMobile = false;
		self.dom.projects.wrapper.find('.project').each(function(){
			/*
			if (isMobile) {
				var maxMargin = 80;
				var marginArray = ['margin-left', 'margin-right'];

				var margin = Math.floor((Math.random() * maxMargin) + 1);
				var randMarginPosition = Math.floor((Math.random() * marginArray.length-1) + 1);

				$(this).css(marginArray[1], margin).addClass('item-initialized');

			} else { */
				function getRand(num){
					return Math.round( (Math.random() * num) * 100 / 100) -10;
				}

				var top = getRand(20), 
					left = getRand(30), 
					bottom = getRand(20), 
					right = getRand(30);

				$(this).css('margin',  	top    + "%" + 
										left   + "%" + 
										bottom + "%" + 
										right  + "%" );
			//}

			$(this).css('max-width', (100 - left - right) + "%");

		});
	},
	enableVideo: function(){ var self = this;
		self.video = {
			element: document.getElementById("bg-video"),
			playing: false,
			play: function(){
				if (self.video.playing == false) {
					console.log('play');
					self.video.element.play();
					self.video.playing = true;
					$(self.video.element).stop(true, false).animate({volume: 1}, 1000);
				}
			},
			pause: function(){
				if (self.video.playing == true) {
					console.log('pause');
					$(self.video.element).stop(true, false).animate({volume: 0}, 6000, function(){
						self.video.element.pause();
						self.video.playing = false;
					});
				}
			},
			init: function(){
				// loop
				self.video.element.onended = function() {
					self.video.element.play();
				};
				$(window).focus();

				// Autoplay the video if application is visible
				if (document.visibilityState == "visible") {
					self.video.element.volume = 0;
					self.video.play();
				}

				// Handle page visibility change events
				document.addEventListener('visibilitychange', function(){
					if (document.visibilityState == "hidden") {
						self.video.pause();
					} else {
						self.video.play();
					}
				}, false);		

				// Inactive
				window.onblur = function(){
					setTimeout(function() {
						self.video.pause();
					}, 1200);
				};

				window.onfocus = function(){
					setTimeout(function() {
						self.video.play();			
					}, 1200);
				};

			}
		}
		self.video.init();
		


		

	},


}


