/* MARIANNE */

var gulp = require('gulp');

var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var connect = require('gulp-connect-php');
var del = require('del');
var filter = require('gulp-filter');
var autoprefixer = require('gulp-autoprefixer');
var babelJs = require('gulp-babel');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var imageMin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var rename = require('gulp-rename');
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');

//yarn add gulp-jshint gulp-sass browser-sync gulp-connect-php del gulp-filter gulp-autoprefixer gulp-babel gulp-uglify gulp-concat gulp-imagemin imagemin-pngquant gulp-rename

var paths = {
	src: './',
};

gulp.task('default', ['init', 'watch']);

gulp.task('init', ['styles', 'scripts', 'connect-sync']);

gulp.task('watch', function() {
	gulp.watch(paths.src + 'assets/sass/**/*.scss', ['styles']);
	gulp.watch(paths.src + 'assets/js/public.js', ['scripts']);
});

gulp.task('styles', function() {
	return gulp
		.src(paths.src + 'assets/sass/**/*.scss')
		.pipe(
			sass({
				//outputStyle: 'compressed'
			}).on('error', sass.logError)
		)
		.pipe(autoprefixer())
		.pipe(gulp.dest(paths.src + 'assets/css/'));
});

gulp.task('scripts', function() {
	gulp
		.src([paths.src + 'assets/js/public.js'])
		.pipe(babelJs())
		.pipe(concat('public.min.js'))
		//.pipe(uglify())
		.pipe(gulp.dest(paths.src + 'assets/js/'));
});

gulp.task('connect-sync', function() {
	connect.server({ base: '.', port: 8010, keepalive: false }, function() {
		browserSync({
			proxy: '127.0.0.1:8010',
			notify: false,
			port: 8015,
			open: true,
		});
	});
});

gulp.task('disconnect', function() {
	connect.closeServer();
});
